# guile-pathlib

A Guile library to handle filepaths.

## Installation

```
autoreconf -i
./configure
make
make install
```

## API

`(path-join-components lst)`

where `lst` is list of paths.

Example:

```scheme
(path-join-components '("/boot" "file.txt"))
;; => "/boot/file.txt"
```

`(path-join paths ...)`

Similar to `path-join-components`, except takes the paths as variadic
arguments.

Example:

```scheme
(path-join "/boot" "file.txt")`
;; => "/boot/file.txt"
```

`(path-find str-or-list subpath [#:delimiter #\:])`

Finds a subpath (file, directory, symlink, etc.) in the list of paths
described by `str-or-list`. If `str-or-list` is a string, then the
string will be split by `delimiter` to get a list of paths. Otherwise
the list of paths will be used as is.
 
Example:

```scheme
(path-find "/usr/local/bin:/usr/bin" "ls")
;; => "/usr/bin/ls"
;; or
;; => #f
```

`(path-dirname path)`

Returns the directory of the path

Example:

```scheme
(path-dirname "/usr/bin/ls")
;; => "/usr/bin"

(path-dirname "file.txt")
;; => ""

(path-dirname "/usr/bin/directory/")
;; => "/usr/bin/directory"
```

`(path-basename path)`

Get the filename of the path.

Example:

```scheme
(path-basename "/usr/bin/directory/")
;; => ""

(path-basename "/usr/bin/directory")
;; => "directory"

(path-basename "file.txt")
;; => "file.txt"
```

`(path-split path)`

Split the path into dirname and basename.

Example:

```scheme
(path-split "/usr/bin/file.txt")
;; => "/usr/bin"
;; => "file.txt"
```

`(path-expanduser path)`

Expand a leading tilde into the home directory if there is one.

Example:

```scheme
(path-expanduser "/usr/bin/file.txt")
;; => "/usr/bin/file.txt"

(path-expanduser "~/file.txt")
;; => "/home/user/file.txt"
```

`(path-splitext "file.txt")`

Splits the path into path

Example:

```
(path-splitext "file.txt")
;; => "file"
;; => ".txt"

(path-splitext "file.txt")
;; => "file"
;; => ""

(path-splitext "/usr/bin/file.txt")
;; => "/usr/bin/file"
;; => ".txt"
```

`(path-without-trailing-slashes path)`

Returns the path without trailing slashes

Example:

```scheme
(path-without-trailing-slashes "/usr/")
;; => "/usr"

(path-without-trailing-slashes "/usr//")
;; => "/usr"
```

`(path-split-components path)`

Split the path into its components.

Example:

```scheme
(path-split-components "/usr/bin/ls")
;; => ("usr" "bin" "ls")

(path-split-components "ls")
;; => ("ls")
```


`(path-xdg-data-home [subpath] [#:create? #f])`\
`(path-xdg-config-home [subpath] [#:create? #f])`\
`(path-xdg-state-home [subpath] [#:create? #f])`\
`(path-xdg-cache-home [subpath] [#:create? #f])`\
`(path-xdg-runtime-dir [subpath] [#:create? #f])`

Finds the appropriate XDG specification path. 

Example:

```
(path-xdg-data-home)
;; => "/home/user/.local/share"

(path-xdg-data-home "application")
;; => "/home/user/.local/share/application"

(path-xdg-data-home "application" #:create? #t)
;; Creates the directory
;; => "/home/user/.local/share/application"
```

`(path-xdg-executable-dir)`

Returns the directory for local executables.

Example:

```scheme
(path-xdg-executable-dir)
;; => "/home/user/.local/bin"
```


`(path-xdg-data-dirs-search subpath)`\
`(path-xdg-config-dirs-search subpath)`

Search XDG data and config dirs for a subpath.

Example:

```scheme
(path-xdg-config-dirs-search "application/config")
;; => "/home/user/.config/application/config"
;; or
;; => #f
```
