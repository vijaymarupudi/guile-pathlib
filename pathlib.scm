(define-module (pathlib)
  #:use-module (srfi srfi-1)
  #:export (path-join-components
            path-join

            path-find

            path-dirname
            path-basename
            path-split

            path-expanduser
            path-splitext

            path-without-trailing-slashes
            path-split-components

            path-xdg-data-home
            path-xdg-config-home
            path-xdg-state-home
            path-xdg-cache-home
            path-xdg-data-dirs-search
            path-xdg-config-dirs-search
            path-xdg-executable-dir
            path-xdg-runtime-dir))

(define (path--index-of-last-slash str)
  (if (= (string-length str) 0)
      #f
      (let loop ((i (- (string-length str) 1)))
        (cond
         ((< i 0) #f)
         ((char=? (string-ref str i) #\/) i)
         (else (loop (- i 1)))))))

(define (path-without-trailing-slashes str)
  (let ((len (string-length str)))
    (if (not (char=? (string-ref str (- len 1)) #\/))
        str
        (let loop ((idx (- len 2)))
          (cond
           ((< idx 0) "")
           ((char=? (string-ref str idx) #\/) (loop (- idx 1)))
           (else (substring str 0 (+ idx 1))))))))


(define (path-basename str)
  (let ((idx (path--index-of-last-slash str)))
    (if (not idx)
        str
        (substring str (+ idx 1)))))

(define (path-dirname str)
  (let ((idx (path--index-of-last-slash str)))
    (if (not idx)
        ""
        (substring str 0 idx))))

(define (path-split str)
  (let ((idx (path--index-of-last-slash str)))
    (if (not idx)
        (values "" str)
        (values (substring str 0 idx)
                (substring str (+ idx 1))))))

(define (path-split-components str)
  (filter (lambda (x)
            (not (= (string-length x) 0)))
          (string-split str #\/)))


(define (path-splitext str)
  (let* ((slash-idx (path--index-of-last-slash str))
         (base-start-idx (if slash-idx (+ slash-idx 1) 0))
         (period-idx (string-index-right str #\. base-start-idx)))
    (cond
     ((not period-idx) (values str ""))
     ;; hidden file optimized special case
     ((= base-start-idx period-idx) (values str ""))
     ((not (string-skip str #\. base-start-idx period-idx))
      ;; the name of the file just starts with leading periods
      ;; leading periods are considered to part of the root
      (values str ""))
     (else
      (values (substring str 0 period-idx)
              (substring str period-idx))))))


(define (path-expanduser str)
  (cond
   ((< (string-length str) 2) str)
   ((and (char=? (string-ref str 0) #\~)
         (char=? (string-ref str 1) #\/))
    (string-append (getenv "HOME") (substring str 1)))
   (else str)))

(define (path-join-components lst)
  (let ((len (length lst)))
    (case len
      ((0) "")
      ((1) (car lst))
      (else (call-with-values
                (lambda () (split-at lst (- len 1)))
              (lambda (preceding-components last-components)
                (string-join (append! (map! path-without-trailing-slashes preceding-components)
                                      last-components)
                             "/")))))))

(define (path-join . paths)
  (path-join-components paths))

(define (path-xdg-dirs-search local dirs-var subpath)
  (let* ((paths (cons local
                      (filter
                       (lambda (str) (not (= (string-length str) 0)))
                       (string-split dirs-var #\:))))
         (existing-path (find (lambda (x)
                                (let ((path (path-join x subpath)))
                                  (access? path F_OK)))
                              paths)))
    (if existing-path
        (values (path-join existing-path subpath) #t)
        (values (path-join local subpath) #f))))

(define* (path-find str-or-list subpath #:key (delimiter #\:))
  (let* ((paths (filter
                 (lambda (x)
                   (not (= (string-length x) 0)))
                 (if (string? str-or-list)
                     (string-split str-or-list delimiter)
                     str-or-list))))
    (let loop ((paths paths))
      (if (null? paths)
          #f
          (let ((path (path-join (car paths) subpath)))
            (if (access? path F_OK)
                path
                (loop (cdr paths))))))))

(define* (path--get-xdg-dir-internal env-name subdir)
  (let ((env-var (getenv env-name)))
    (if env-var
        (path-without-trailing-slashes env-var)
        (path-join (getenv "HOME") subdir))))


(define-syntax-rule (path--define-home-variable-functions (name env-name subdir) ...)
  (begin
    (define* (name #:optional subpath #:key (create? #f))
      (let ((dir (if subpath
                     (path-join (path--get-xdg-dir-internal env-name subdir) subpath)
                     (path--get-xdg-dir-internal env-name subdir))))
        (when create?
          (system* "mkdir" "-p" dir))
        dir)) ...))

(path--define-home-variable-functions
  (path-xdg-data-home   "XDG_DATA_HOME" ".local/share")
  (path-xdg-config-home "XDG_CONFIG_HOME" ".config")
  (path-xdg-state-home  "XDG_STATE_HOME" ".local/state")
  (path-xdg-cache-home  "XDG_CACHE_HOME" ".cache"))

(define (path-xdg-executable-dir)
  (path-join (getenv "HOME") ".local/bin"))

(define-syntax-rule (path--define-dirs-variable-functions name env-name default-env-value standard-value-thunk)
  (define (name subpath)
    (let* ((paths-env-variable (let ((env-value (getenv env-name)))
                                 (if env-value
                                     env-value
                                     default-env-value)))
           (paths-string (string-append (standard-value-thunk) ":" paths-env-variable))
           (found-path (path-find paths-string subpath #:delimiter #\:)))
      found-path)))

(path--define-dirs-variable-functions path-xdg-config-dirs-search "XDG_CONFIG_DIRS"
  "/etc/xdg" path-xdg-config-home)

(path--define-dirs-variable-functions path-xdg-data-dirs-search "XDG_DATA_DIRS"
  "/usr/local/share/:/usr/share/" path-xdg-data-home)

(define* (path-xdg-runtime-dir #:optional subpath #:key (create? #f))
  (let* ((env (getenv "XDG_RUNTIME_DIR"))
         (dir (path-join-components (filter identity (list (if env env "/tmp") subpath)))))
    (when create?
      (system* "mkdir" "-p" dir))
    dir))
